# form_builder
Builds the HTML for a web form from a schema.
Plays well with jsdom (See example)

[![Build Status](https://travis-ci.org/BebeSparkelSparkel/form_builder.svg?branch=master)](https://travis-ci.org/BebeSparkelSparkel/form_builder)

## Install Require
```sh
npm install https://github.com/BebeSparkelSparkel/form_builder.git
```
```javascript
const form_builder = require('form_builder')
```

## Form Builder Function
function **form_builder**(schema, options)  
*schema*: Array of Objects. See below section *Schema* for details.
*options*  
- *form_attributes*: Object with the form element's attributes. Default `{method: 'post'}`  
- *submit_text*: Text that will be put in the submit input will display (input.value). If false, submit button will not be insterted. Default "Submit"  
Returns the Form Element  

## Non Global document
If there is not a global document because this is running in node you can set it with:  
```javascript
const form_builder = require('form_builder')
form_builder.document = document
```

## Schema
An Array of Objects that defines what will be put in the form.  

### Schema Array Objects' Properties
**name** (required)  
- String of name of the input element  
- Added as an attribute to the input element  

**label** (optional)  
- String of label element before the input element  
- If not given no label is inserted  

**attr** (optional)  
- Object of html attributes to add to the input element  
- keys are the attribute type  
- values are the attribute value  

**group** (optional)  
- String that groups inputs of the same group into a `div` element  

### Schema Example
```javascript
const schema = [
  {
    name: 'first_name',
    label: 'First Name', // optional
    attr: {type: 'text'}, // optional
  },
  {
    name: 'last_name',
    label: 'Last Name', // optional
    attr: {type: 'text', required: true}, // optional
  },
]
```

## form_builder Example
```javascript
const { JSDOM } = require('jsdom')
const { window } = new JSDOM('<!DOCTYPE html><body></body>')
const { document } = window.window

const form_builder = require('form_builder')
form_builder.document = document


const schema = [
  {
    name: 'first_name',
    label: 'First Name', // optional
    attr: {type: 'text'}, // optional
  },
  {
    name: 'last_name',
    label: 'Last Name', // optional
    attr: {type: 'text', required: true}, // optional
  },
]

const form_element = form_builder(schema)

const html = form_element.outerHTML
console.log(html)
```
Result
```html
<form method="post">
  <div>
    <label>First Name</label>
    <input name="first_name" type="text">
    <p class="input-error-msg"></p>
  </div>

  <div>
    <label>Last Name</label>
    <input name="last_name" type="text" required="true">
    <p class="input-error-msg"></p>
  </div>
  
  <input name="submit" type="submit" value="Submit">
</form>
```

## To Do
- Allow for a mustashe template to be inputted and filled in.
