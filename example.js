/*eslint-disable no-console */

const { JSDOM } = require('jsdom')
const { window } = new JSDOM('<!DOCTYPE html><body></body>')
const { document } = window.window

const form_builder = require('form_builder')
form_builder.document = document


const schema = [
  {
    name: 'first_name',
    label: 'First Name', // optional
    attr: {type: 'text'}, // optional
  },
  {
    name: 'last_name',
    label: 'Last Name', // optional
    attr: {type: 'text', required: true}, // optional
  },
]

const form_element = form_builder(schema)

const html = form_element.outerHTML
console.log(html)

