/*eslint-disable no-console */

/*
Returns a form element that has been built from schema.

schema (required) - array of objects that contain the properties:
  name (required) - String of name of the input element
                    added as an attribute to the input element
  label (optional) - String of label element before the input element
                     if not given no label is inserted
  attr (optional) - Object of html attributes to add to the input element
                    keys are the attribute type
                    values are the attribute value

Example schema:
[
  {
    name: 'first_name',
    label: 'First Name',
    attr: {type: 'text'},
  },
  {
    name: 'last_name',
    label: 'Last Name',
    attr: {type: 'text'},
  },
]
*/


function form_builder(schema,
  {
    form_attributes={method: 'post'},
    submit_text='Submit',
    form=form_builder.document.createElement('form'), // form element
  }={} // options
) {

  // set form element attributes
  for (const attr in form_attributes) {
    if (form_attributes.hasOwnProperty(attr)) {
      form.setAttribute(attr, form_attributes[attr])
    }
  }

  const group_containers = {}
  // why "function" is used here: https://github.com/sublimehq/Packages/issues/1430
  schema.forEach(function({
    label: label_text = '',
    name,
    attr: attributes = {},
    group,
  }) {
    if (name === undefined) throw new Error('No name specified for input.')

    const input_div = (() => {
      if (group !== undefined) {
        if (group_containers[group] === undefined)
          group_containers[group] = createAppendElement(form, 'div')

        return createAppendElement(group_containers[group], 'div')
      }

      return createAppendElement(form, 'div')
    })()

    // set label text
    createAppendElement(input_div, 'label').innerHTML = label_text

    // create input and apply its attributes
    const input = createAppendElement(input_div, 'input')
    input.name = name
    for (const attr in attributes) {
      if (attributes.hasOwnProperty(attr)) {
        const attr_value = attributes[attr]

        input.setAttribute(attr, attr_value)

        if (attr === 'type' && attr_value.toLowerCase() === 'file')
          form.enctype = 'multipart/form-data'
      }
    }

    // error text
    createAppendElement(input_div, 'p').classList.add('input-error-msg')
  })

  // submit
  if (submit_text !== false) {
    const submit = createAppendElement(form, 'input')
    submit.type = submit.name = 'submit'
    submit.value = submit_text
  }

  return form
}


function createAppendElement(parent, tag) {
  /*
  Creates a new element of type tag.
  Appends new element to parent.
  Returns new element.

  parent - element that the new element is appended to
  tag - string that defines the new element's tag
  */
  const new_element = form_builder.document.createElement(tag)
  parent.appendChild(new_element)
  return new_element
}


try {
  form_builder.document = document
} catch(err) {
  form_builder.document = undefined
}


module.exports = form_builder
